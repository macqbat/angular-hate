import { Routes } from '@angular/router';
import { HomeComponent } from './home';
import { HeaderComponent } from './core/header.component.ts';
import { FooterComponent } from './core/footer.component.ts';
import { NoContentComponent } from './no-content';

import { DataResolver } from './app.resolver';

export const ROUTES: Routes = [
  { path: '',      component: HomeComponent },
  { path: '**',    component: NoContentComponent },
  { path: '', component: HeaderComponent, outlet: 'header'},
  { path: '', component: FooterComponent, outlet: 'footer'},

];
