import {
    ModuleWithProviders, NgModule,
    Optional, SkipSelf, CUSTOM_ELEMENTS_SCHEMA
} from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FooterComponent } from './footer.component';
import { HeaderComponent } from './header.component';
import { SpinnerComponent } from './spinner.component';

@NgModule({
    imports: [CommonModule, RouterModule, FormsModule],
    declarations: [FooterComponent,
        HeaderComponent,
        SpinnerComponent],
    exports: [FooterComponent,
        HeaderComponent,
        RouterModule,
        SpinnerComponent],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class CoreModule {}
