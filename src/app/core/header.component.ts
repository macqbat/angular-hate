import { Component, Input, OnInit, AfterViewInit } from '@angular/core';

declare var $: any;

@Component({
    selector: 'app-navbar',
    template: `
        <nav class="orange darken-3">
          <div class="nav-wrapper">
            <a href="#" class="brand-logo left">Coś tam</a>
            <ul id="nav-mobile" class="right">
              <li class="nav-wrapper__welcome"> </li>
              <li class="login" (click)="onLoginClick()">
                <span class="btn">
                  <span *ngIf="!login">login</span>
                  <span *ngIf="login">logout</span>
                </span>
              </li>
            </ul>
          </div>
        </nav>
    `,
    styles: [`
      .login{
        cursor: pointer;

      }
    `]
})

export class HeaderComponent {
  login: boolean;
  onLoginClick(){
    this.login = !this.login;
  }
}
