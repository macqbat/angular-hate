import { Component } from '@angular/core';

@Component({
    selector: 'app-footer',
    template: `
        <div class="container">
          <div class="row">
            <div class="col l6 s12">
              <h5 class="white-text">Angular love</h5>
              <p class="grey-text text-lighten-4"></p>
            </div>
            <div class="col l4 offset-l2 s12">
              <h5 class="white-text">Links</h5>
              <ul>
                <li><a class="grey-text text-lighten-3"
                  href="#">link</a></li>
                <li><a class="grey-text text-lighten-3"
                  href="#">link</a></li>
                <li><a class="grey-text text-lighten-3"
                  href="#">Link</a></li>
                <li><a class="grey-text text-lighten-3"
                  href="#">A link</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="footer-copyright">
          <div class="container">
            © 2017 Copyright Text
            <!-- a class="grey-text text-lighten-4 right" href="#!">More Links</a -->
          </div>
        </div>
    `,
})
export class FooterComponent {}
