import { Component, Input, OnInit, OnDestroy } from '@angular/core';

import { CourseListItem } from './courses.models';


@Component({
    selector: 'course-listitem',
    template: `
      <div class="col s10">
        <div class="col s12">
          <h3 class="col s5">{{course.name}}</h3>
          <span class="col s4">{{course.duration}}</span>
          <span class="col s3 right">{{course.date | date:'medium'}}</span>
        </div>
        <div class="col s12">
          {{course.description}}
        </div>
      </div>
      <div class="col s2 course-buttons">
        <button class="waves-effect waves-light btn col s12">Edit course</button>
        <button class="waves-effect waves-light btn col s12">Delete</button>
      </div>`
})

export class CourseItemComponent {
  @Input() course: CourseListItem;
}
