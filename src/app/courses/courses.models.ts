export interface CourseListItem{
    id: string;
    name: string;
    description?: string;
    duration?:  number;
    date?:  number;
}

export interface Course{
  id: string;
  name: string;
  description?: string;
  duration?:  number;
  date?:  number;
  category?: any;
  longDescription?: string;
  authors?: any;
}

export interface CourseList{
  items: CourseListItem[];
  total: number;
}
