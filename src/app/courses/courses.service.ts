import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { CourseList, CourseListItem, Course } from './courses.models';

@Injectable()
export class CoursesService {

  getCourses(): Observable<CourseList>{

    let courses: CourseListItem[] = [
    {id: 'raz',
      name: 'Video łan',
      description: 'Lorem donor ipsut...',
      duration: 333,
      date: (new Date()).valueOf()
    },
    {id: 'dwa',
      name: 'Video sekond',
      description: 'Lorem donor ipsut...',
      duration: 666,
      date: (new Date()).valueOf()
    },
    {id: 'piec',
      name: 'Video sri',
      description: 'Lorem donor ipsut...',
      duration: 767,
      date: (new Date()).valueOf()
    }
    ]
    let items:CourseList = {items: courses, total: courses.length};

    return Observable.of(items);
  }
}
