import { Component, Input, OnInit, OnDestroy } from '@angular/core';

import { CoursesService } from './courses.service';
import { CourseListItem } from './courses.models';


@Component({
    selector: 'app-courses',
    templateUrl: './courses.component.html',
})

export class CoursesComponent implements OnInit{

  courses: CourseListItem[];
  total: number;

  constructor(private coursesService: CoursesService){}

  ngOnInit(){
    this.coursesService
      .getCourses()
      .subscribe(data =>{
        this.courses = data.items;
        this.total = data.total;
      });
  }
}
