import {
  Component,
  OnInit
} from '@angular/core';

@Component({
  selector: 'toolsbelt-home',
  template: `
  <div class="col s12 toolsbelt">
    <div class="card">
      <div class="card-content toolsbelt__card">
        <div class="col s10 toolsbelt__card__search">
          <input type="search" class="col s7">
          <button class="waves-effect waves-light btn col s2">Find</button>
        </div>
        <div class="col s2">
          <button class="waves-effect waves-light btn col s12">Add course</button>
        </div>
        <br />
      </div>
    </div>
  </div>
  `
})
export class ToolsBeltComponent {

}
