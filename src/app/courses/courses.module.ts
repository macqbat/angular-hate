import {
    ModuleWithProviders, NgModule,
    Optional, SkipSelf, CUSTOM_ELEMENTS_SCHEMA
} from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { CoursesComponent } from './courses.component';
import { CourseItemComponent } from './course-item.component';
import { ToolsBeltComponent } from './tools.component';
import { CoursesService } from './courses.service';

@NgModule({
    imports: [CommonModule, RouterModule, FormsModule],
    declarations: [CoursesComponent, CourseItemComponent, ToolsBeltComponent],
    exports: [CoursesComponent, CourseItemComponent, ToolsBeltComponent],
    providers: [CoursesService],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class CoursesModule {}
