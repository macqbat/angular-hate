/*
 * Angular 2 decorators and services
 */
import {
  Component,
  OnInit,
  ViewEncapsulation
} from '@angular/core';
import { AppState } from './app.service';

@Component({
  selector: 'body',
  encapsulation: ViewEncapsulation.None,
  styleUrls: [
    './app.component.css'
  ],
  template: `
    <header>
      <router-outlet name="header"></router-outlet>
    </header>
    <main class="row">
      <div class="col s10 offset-s1">
      <router-outlet></router-outlet>
      <pre class="app-state">this.appState.state = {{ appState.state | json }}</pre>
      </div>
    </main>
    <footer class="page-footer orange darken-3">
      <router-outlet name="footer"></router-outlet>
    </footer>
  `
})
export class AppComponent implements OnInit {
  public angularclassLogo = 'assets/img/angularclass-avatar.png';
  public name = 'Angular 2 Webpack Starter';
  public url = 'https://twitter.com/AngularClass';

  constructor(
    public appState: AppState
  ) {}

  public ngOnInit() {
    console.log('Initial App State', this.appState.state);
  }

}
